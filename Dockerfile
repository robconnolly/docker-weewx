FROM python:3-alpine as stage-1

ARG WEEWX_UID=421
ENV WEEWX_HOME="/home/weewx"
ENV WEEWX_VERSION="4.7.0"
ENV ARCHIVE="weewx-${WEEWX_VERSION}.tar.gz"

RUN addgroup --system --gid ${WEEWX_UID} weewx \
  && adduser --system --uid ${WEEWX_UID} --ingroup weewx weewx

RUN apk --no-cache add tar build-base py-pip jpeg-dev zlib-dev bash su-exec freetype-dev

WORKDIR /tmp
COPY src/hashes requirements.txt neowx-material-matomo-autorefresh.patch ./

# Download sources and verify hashes
RUN wget -q -O "${ARCHIVE}" "http://www.weewx.com/downloads/released_versions/${ARCHIVE}"
RUN wget -q -O weewx-mqtt.zip https://github.com/matthewwall/weewx-mqtt/archive/master.zip
RUN wget -q -O WeeWX-MQTTSubscribe.zip https://github.com/bellrichm/WeeWX-MQTTSubscribe/archive/v2.0.0.zip
RUN wget -q -O neowx-material-latest.zip https://neoground.com/projects/neowx-material/download/latest
RUN sha256sum -c < hashes

# WeeWX setup
RUN tar --extract --gunzip --directory ${WEEWX_HOME} --strip-components=1 --file "${ARCHIVE}"
RUN chown -R weewx:weewx ${WEEWX_HOME}

# Python setup
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
RUN pip install --no-cache-dir --requirement requirements.txt

WORKDIR ${WEEWX_HOME}

RUN bin/wee_extension --install /tmp/weewx-mqtt.zip
RUN bin/wee_extension --install /tmp/WeeWX-MQTTSubscribe.zip
RUN bin/wee_extension --install /tmp/neowx-material-latest.zip
COPY src/entrypoint.sh src/version.txt ./

# Patch neowx-material skin
WORKDIR ${WEEWX_HOME}/skins/neowx-material
RUN patch -p1 < /tmp/neowx-material-matomo-autorefresh.patch
WORKDIR ${WEEWX_HOME}

VOLUME ["/data", "/home/weewx/public_html"]

ENV PATH="/opt/venv/bin:$PATH"
ENTRYPOINT ["./entrypoint.sh"]
CMD ["/data/weewx.conf"]
